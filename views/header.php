<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<base href="<?php echo base(); ?>">
	<link rel="shortcut icon" type="image/png" href="public/img/favicon.png">
	<link href="public/css/styles.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
	<title>FileTransfer</title>
</head>

<body>
	<header id="main-header">
		<a href=".">
			<img id="logo" src="public/img/logo.svg">
		</a>
	</header>

	<div id="overlay">
		<div class="window">
			<p id="wrong1"></p>
			<p id="wrong2"></p>
		</div>
	</div>

	<main>
