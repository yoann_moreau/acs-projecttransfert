<?php 
	require "views/header.php";
?>

<section class="window" id="404">
	<h1>ERREUR 404</h1>
	<p>Si vous êtes sur cette page c'est que vous êtes victime du virus 404.</p>
	<p>Pensez à retourner sur l'accueil en cliquant sur le logo !</p>
</section>

<?php 
	require "views/footer.php";
?>
