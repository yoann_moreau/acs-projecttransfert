<?php 
	require "views/header.php";
?>

<section id="info" class="window">
	<p>Transférez vos fichiers en toute simplicité (jusqu'à 4Go) !</p>
</section>

<section id="transfer">
	<section id="file-info" class="window">
		<article id="upload">
			<div id="drop-zone">
				<input type="file" name="upload-file[]" id="upload-file" form="form" data-multiple-caption="{count} fichiers sélectionnés" multiple>
				<label for="upload-file" id="label-upload">+</label>
			</div>
			<div class="info">
				<p id="upload-status">Cliquez pour ajouter vos fichiers.</p>
			</div>
		</article>
	</section>
	<section id="upload-info" class="window">
		<form id="form" method="post" action="." enctype="multipart/form-data">
			<input type="text" name="email-sender" id="email-sender" placeholder="Email expéditeur">
			<input type="text" name="email-receiver" id="email-receiver" placeholder="Email destinataire">
			<textarea name="message" autocomplete="off" placeholder="Message"></textarea>
			<input type="submit" name="submit-button" id="submit-button">
		</form>
	</section>
</section>

<?php 
	require "views/footer.php";
?>
