<?php

require_once('tools/bdd.php');

function storeSender($sender) {
  global $bdd;
  $sql = "SELECT id_sender FROM senders WHERE email_sender = :sender";

  $query = $bdd->prepare($sql);
  $query->execute(['sender' => $sender]);

  $result = $query->fetch();

  if (empty($result)) {
    $sql = "INSERT INTO senders VALUES (NULL, :sender)";

    $query = $bdd->prepare($sql);
    $query->execute(['sender' => $sender]);

    $query = $bdd->query('SELECT LAST_INSERT_ID()');
    $result = $query->fetch();

    return $result['LAST_INSERT_ID()'];
  }
	return $result['id_sender'];
}

function storeReceiver($receiver) {
  global $bdd;
  $sql = "SELECT id_receiver FROM receivers WHERE email_receiver = :receiver";

  $query = $bdd->prepare($sql);
  $query->execute(['receiver' => $receiver]);

  $result = $query->fetch();

  if (empty($result)) {
    $sql = "INSERT INTO receivers VALUES (NULL, :receiver)";

    $query = $bdd->prepare($sql);
    $query->execute(['receiver' => $receiver]);

    $query = $bdd->query('SELECT LAST_INSERT_ID()');
    $result = $query->fetch();

    return $result['LAST_INSERT_ID()'];
  }
	return $result['id_receiver'];
}

function storeMessage($message) {
  global $bdd;
  $sql = "SELECT id_message FROM messages WHERE message = :message";

  $query = $bdd->prepare($sql);
  $query->execute(['message' => $message]);

  $result = $query->fetch();

  if (empty($result)) {
    $sql = "INSERT INTO messages VALUES (NULL, :message)";

    $query = $bdd->prepare($sql);
    $query->execute(['message' => $message]);

    $query = $bdd->query('SELECT LAST_INSERT_ID()');
    $result = $query->fetch();

    return $result['LAST_INSERT_ID()'];
  }
	return $result['id_message'];
}

function storeFile($name, $idSender, $idMessage, $link) {
  global $bdd;
  $sql = "INSERT INTO files VALUES (NULL, 0, :name, NOW(), DATE_ADD(NOW(), INTERVAL 1 Week), :idSender, :idMessage, :link)";

  $query = $bdd->prepare($sql);
  $query->execute(['name' => $name, 'idSender' => $idSender, 'idMessage' => $idMessage, 'link' => $link]);

  $query = $bdd->query('SELECT LAST_INSERT_ID()');

  $result = $query->fetch();
  return $result['LAST_INSERT_ID()'];
}

function joinReceiver($idReceiver, $idFile) {
  global $bdd;
  $sql = "INSERT INTO have_receiver VALUES (:idReceiver, :idFile)";

  $query = $bdd->prepare($sql);
  $query->execute(['idReceiver' => $idReceiver, 'idFile' => $idFile]);
}

function storeLink()
{
	global $bdd;
  $sql = "SELECT link FROM files WHERE link = :link";
  $stmt = $bdd->prepare($sql);
	do
	{
		$gen = generate();
		$stmt->execute(['link' => $gen]);
  	$result = $stmt->fetch();
	}
	while (!empty($result));

	return $gen;
}

function generate()
{
	$alpha = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];

	$str = '';
	for ($i=0; $i<16; $i++)
	{
		$x = random_int(0,35);
		$str .= $alpha[$x];
	}
	return $str;
}

function downloadFile($link){
	global $bdd;
	$sql = 'SELECT  file, file_name, link FROM files WHERE link = :link';

	$query =  $bdd->prepare($sql);
  $query->execute(['link' => $link]);
	$item = $query->fetch();
	// $list = [];
	// array_push($list, $item['file']);
	// array_push($list, $item['file_name']);

	return [
		'name' => $item['file_name'],
		'data' => $item['file'],
    'link' => $item['link']
	];

}

function DeleteFile(){
	global $bdd;
	$sql = 'DELETE * FROM files WHERE expiration_date < NOW()';
	$stmt =  $bdd->prepare($sql);
	$stmt->execute();
}