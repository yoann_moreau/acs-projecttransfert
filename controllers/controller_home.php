<?php
require('models/model.php');

if (isset($_POST['submit-button']))
{
	require('views/loading.php');
	//check for empty fields
	if ( !empty($_POST['email-sender'])
		&& !empty($_POST['email-receiver'])
		&& !empty($_FILES['upload-file']['name']))
	{
		$sender = trim(htmlspecialchars($_POST['email-sender']));
		$receiver = trim(htmlspecialchars($_POST['email-receiver']));
		$message = trim(addslashes(htmlspecialchars($_POST['message'])));

		$folder = 'upload/';
		// Create new zip
		$zip = new ZipArchive();
		$link = storeLink();
		$zip_name = $link.'.zip';
		$zip_path = $folder.$zip_name;
		if($zip->open($zip_path, ZIPARCHIVE::CREATE)!==TRUE)
		{
		  echo "Désolé, la création du zip a échoué.";
		}

		// For each file, upload it, then add it to the zip
		$files_number = count($_FILES['upload-file']['name']);
		for ($i = 0; $i < $files_number; $i++)
		{
			$file_name = $_FILES['upload-file']['name'][$i];
			move_uploaded_file($_FILES['upload-file']['tmp_name'][$i],
				$folder.$_FILES['upload-file']['name'][$i]);
			$zip->addFile($folder.$file_name);
		}
		// Close zip
		$zip->close();

		// delete files from upload directory
		for ($i = 0; $i < $files_number; $i++)
		{
			$file_name = $_FILES['upload-file']['name'][$i];
			unlink($folder.$file_name);
		}

		$size_file = filesize($zip_path);

		//check for uncongruent emails
		if (preg_match('#^[\w.-]+@[\w.-]{2,}\.[a-z]{2,6}$#', $sender) == true
		 && preg_match('#^[\w.-]+@[\w.-]{2,}\.[a-z]{2,6}$#', $receiver) == true)
		{
			if ($size_file < 2000000)	//check for too heavy files
			{
				// Store sender, receiver and message in db and get their ids
				$idSender = storeSender($sender);
				$idReceiver = storeReceiver($receiver);
				$idMessage = storeMessage($message);

				// Store file info in DB and get id of file
				$idFile = storeFile($zip_name, $idSender, $idMessage, $link);

				// link receivers and files
				joinReceiver($idReceiver, $idFile);

				// send mail to download page
				$linkBase = base() . "download/" . $link ;

				$messages = 'Nom de fichier : '. $zip_name . PHP_EOL;
				$messages .= 'Expediteur : ' . $sender . PHP_EOL;
				$messages .= 'Destinataire : '. $receiver . PHP_EOL;
				$messages .= 'Message : '. $message . PHP_EOL;
				$messages .= 'Téléchargement : ' . $linkBase ;

				$subject = 'Fichier disponible sur FileTransfer';

				mail($sender,$subject,$messages);
				mail($receiver,$subject,$messages);

				// Redirect towards result page
				header('location: result/'.$link);

			}
		}
	}
}
else
{
	require('views/home.php');
}
