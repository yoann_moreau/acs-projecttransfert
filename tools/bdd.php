<?php
require_once('tools/connection.php');

$db = connect()['BDD'];
$login = connect()['LOGIN'];
$pswd = connect()['PSWD'];

try {
  $bdd = new PDO('mysql:host=localhost;dbname='.$db.';charset=utf8', $login, $pswd);
	$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch (Exception $e)
{
  die('Erreur : ' .$e->getMessage());
}
